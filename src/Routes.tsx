import React, { useContext, useEffect } from 'react';
import { Route, RouteObject, useRoutes } from 'react-router-dom';
import { getMyUser } from './services/user';
import { Context } from './Store';
import useWrapFetch from './hooks/useWrapFetch';
import ExamplePage from './pages/Example';
import PrivateRoute from './PrivateRouter';
import Example2Page from './pages/Example2';
import DashboardPage from './pages/Dashboard';
import ErrorPage from './pages/Error';
import Loading from './components/Loading';

const AppRoutes: React.FC = () => {
    // load the current user
    const [state, dispatch] = useContext(Context);
    const getMyUserWrapped = useWrapFetch(getMyUser)();
    useEffect(() => {
        getMyUserWrapped((user) => dispatch({ type: 'SET_USER', payload: user }));
    }, []);
    const { isLoading } = state;
    const isLoggedIn = !!state.user;
    const routes: RouteObject[] = [
        {
            path: '/*',
            index: true,
            element: <PrivateRoute component={DashboardPage} />, // TODO: change to the default path to go when no path match eg: Dashboard
        },
        {
            path: '/example/*',
            element: <PrivateRoute component={ExamplePage} />,
        },
        {
            path: '/example2/*',
            element: <PrivateRoute component={Example2Page} />,
        },
        {
            path: '/error/*',
            element: <ErrorPage />,
        },
    ];
    const routesElement = useRoutes(routes);

    if (isLoading) {
        return <Loading />;
    }
    if (!isLoggedIn && isLoading) {
        return <div />;
    }

    return routesElement;
};

export default AppRoutes;
