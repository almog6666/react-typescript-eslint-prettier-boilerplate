import React, { useContext, useMemo } from 'react';
import './App.css';
import { BrowserRouter } from 'react-router-dom';
import { ThemeProvider, StylesProvider, jssPreset, createTheme } from '@material-ui/core/styles';
import { create } from 'jss';
import rtl from 'jss-rtl';
import theme from './Theme';
import Store, { Context } from './Store';
import Routes from './Routes';

// Configure JSS
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const App: React.FC = () => {
    const [state] = useContext(Context);
    const appTheme = useMemo(
        () =>
            createTheme({
                palette: {
                    type: state.isDarkMode ? 'dark' : 'light',
                },
            }),
        [state.isDarkMode]
    );
    const finalTheme = { ...theme, ...appTheme };

    return (
        <Store>
            <ThemeProvider theme={finalTheme}>
                <StylesProvider jss={jss}>
                    <BrowserRouter>
                        <Routes />
                    </BrowserRouter>
                </StylesProvider>
            </ThemeProvider>
        </Store>
    );
};

export default App;
