const config = {
    endpoints: {
        user: {
            api: process.env.REACT_APP_ENV === 'local' ? 'http://localhost:5000/api/user' : '/api/user',
        },
    },
};

export default config;
